# Cudarun
usage: cudarun [-h] [--discrete] [--verbose] ...

positional arguments:

  command         Command to execute

optional arguments:

  -h, --help      show this help message and exit
  
  --discrete, -d  Activate a discrete card
  
  --verbose, -v   Print steps
